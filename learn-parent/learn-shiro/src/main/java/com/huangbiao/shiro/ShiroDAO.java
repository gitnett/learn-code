package com.huangbiao.shiro;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

public class ShiroDAO {
	
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * 根据用户名查询密码
	 */
	public String getPasswordByUserName(String username) {
		String sql = "select PASSWORD from SHIRO_USER where username = ?";
		String obj = null;
		try {
			 obj  = jdbcTemplate.queryForObject(sql, String.class, username);
		} catch (EmptyResultDataAccessException e) {
			return "";
		}
		return obj;
	}
	
	/**
	 * 根据用户名查询密码
	 */
	public List<String> getRoleByUserName(String username) {
		String sql = "select ROLE_NAME from SHIRO_ROLE where username = ?";
		List<String> obj = null;
		try {
			 obj  = jdbcTemplate.queryForList(sql, String.class, username);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		return obj;
	}


	/**
	 * 查询当前用户对应的权限
	 */
	public List<String> getPermissionByUserName(String username) {
		String sql = "select P.PERMISSION_NAME from SHIRO_ROLE_PERMISSION P inner join SHIRO_ROLE R on P.ROLE_NAME=R.ROLE_NAME where R.username = ?";
		List<String> perms = jdbcTemplate.queryForList(sql, String.class, username);
		return perms;
	}

}
