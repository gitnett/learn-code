package com.huangbiao.shiro;

import java.util.List;

public interface IShiroService {
	public void doLogin(String username, String password) throws Exception;
	
	/**
	 * 根据用户名查询密码
	 */
	public String getPasswordByUserName(String username);
	/**
	 * 查询用户所有权限
	 */
	public List<String> getPermissionByUserName(String username) ;
	
	public List<String> getRoleByUserName(String username) ;
	
	public String testRoles( );
}
