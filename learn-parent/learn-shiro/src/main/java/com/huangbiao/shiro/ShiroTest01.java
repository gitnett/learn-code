package com.huangbiao.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.Ini;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class ShiroTest01 {
	
	private static final Logger logger = LoggerFactory.getLogger(ShiroTest01.class);
	public static void main(String[] args) {

		Ini ini = Ini.fromResourcePath("classpath:shiro.ini");
		IniSecurityManagerFactory factory = new IniSecurityManagerFactory(ini);
		SecurityManager manager = (SecurityManager) factory.getInstance();
		SecurityUtils.setSecurityManager(manager);
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken("test", "123456", true);
		try {

			subject.login(token);
		} catch (UnknownAccountException uae) {
			logger.info(token.getPrincipal() + "账户不存在");
		} catch (IncorrectCredentialsException ice) {
			logger.info(token.getPrincipal() + "密码不正确");
		} catch (LockedAccountException lae) {
			logger.info(token.getPrincipal() + "用户被锁定了 ");
		} catch (AuthenticationException ae) {
			// 无法判断是什么错了
			logger.info(ae.getMessage());
		}
		logger.info("",subject.hasRole("admin"));
		logger.info("",subject.hasRole("role1"));
		logger.info("",subject.isPermitted("perm6"));
	}
}
